package xyz.derkades.serverselectorx;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import xyz.derkades.derkutils.bukkit.ItemBuilder;

public class OnJoinListener implements Listener {
	
	@EventHandler(priority = EventPriority.LOW)
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		FileConfiguration config = Main.getConfigurationManager().getConfig();
		
		if (config.getBoolean("clear-inv", false)) {
			event.getPlayer().getInventory().clear();
		}
		
		if (config.getBoolean("speed-on-join", false)) {
			int amplifier = Main.getConfigurationManager().getConfig().getInt("speed-amplifier", 3);
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, amplifier, true, false));
		}
		
		if (config.getBoolean("hide-self-on-join", false)) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, true, false));
		}
		
		if (config.getBoolean("hide-others-on-join", false)) {
			InvisibilityToggle.hideOthers(player);
		}
		
		for (FileConfiguration menuConfig : Main.getConfigurationManager().getAll()) {			
			boolean putItemInInventory = menuConfig.getBoolean("on-join");
			if (!putItemInInventory)
				continue;
			
			
			
			if (Main.getPlugin().getConfig().getBoolean("permissions-enabled")) {
				if (!player.hasPermission("ssx.join." + menuConfig.getName().replace(".yml", ""))) {
					continue;
				}
			}
			
			if (menuConfig.getString("item").equals("NONE")) { 
				continue;
			}
			
			Material material = Material.getMaterial(menuConfig.getString("item"));
			
			if (material == null) {
				material = Material.STONE;
			}
			
			ItemBuilder builder = new ItemBuilder(material)
					.data(menuConfig.getInt("data", 0))
					.coloredName(menuConfig.getString("item-name", "error"));
			
			List<String> lore = menuConfig.getStringList("item-lore");
			
			//Don't add lore if it's null or if the first line is equal to 'none'
			if (!(lore == null || lore.isEmpty() || lore.get(0).equalsIgnoreCase("none"))) {
				builder.coloredLore(lore);
			}
			
			int slot = menuConfig.getInt("inv-slot", 0);
			PlayerInventory inv = player.getInventory();
			if (slot < 0) {
				if (!inv.containsAtLeast(builder.create(), 1)) {
					inv.addItem(builder.create());
				}
			} else {
				inv.setItem(slot, builder.create());
			}
			
		}
	}

}
